﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using uHomography;

public class SaveHomography : MonoBehaviour
{

    private Transform vertex1;
    private Transform vertex2;
    private Transform vertex3;
    private Transform vertex4;
    // Use this for initialization
    void Start()
    {
        vertex1 = transform.GetChild(0).transform;
        vertex2 = transform.GetChild(1).transform;
        vertex3 = transform.GetChild(2).transform;
        vertex4 = transform.GetChild(3).transform;
        LoadPositions();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SavePositions()
    {
        PlayerPrefs.SetFloat("v1_x", vertex1.localPosition.x);
        PlayerPrefs.SetFloat("v1_y", vertex1.localPosition.y);
        PlayerPrefs.SetFloat("v2_x", vertex2.localPosition.x);
        PlayerPrefs.SetFloat("v2_y", vertex2.localPosition.y);
        PlayerPrefs.SetFloat("v3_x", vertex3.localPosition.x);
        PlayerPrefs.SetFloat("v3_y", vertex3.localPosition.y);
        PlayerPrefs.SetFloat("v4_x", vertex4.localPosition.x);
        PlayerPrefs.SetFloat("v4_y", vertex4.localPosition.y);
    }

    public void ResetPositions()
    {
        vertex1.localPosition = new Vector3(-4, -5, vertex1.localPosition.z);
        vertex2.localPosition = new Vector3(-4, 5, vertex1.localPosition.z);
        vertex3.localPosition = new Vector3(4, -5, vertex1.localPosition.z);
        vertex4.localPosition = new Vector3(4, 5, vertex1.localPosition.z);
        vertex1.GetComponent<DraggableVertex>().hasChanged = true;
        vertex2.GetComponent<DraggableVertex>().hasChanged = true;
        vertex3.GetComponent<DraggableVertex>().hasChanged = true;
        vertex4.GetComponent<DraggableVertex>().hasChanged = true;

    }

    public void LoadPositions()
    {
        vertex1.localPosition = new Vector3(PlayerPrefs.GetFloat("v1_x"), PlayerPrefs.GetFloat("v1_y"), vertex1.localPosition.z);
        vertex2.localPosition = new Vector3(PlayerPrefs.GetFloat("v2_x"), PlayerPrefs.GetFloat("v2_y"), vertex1.localPosition.z);
        vertex3.localPosition = new Vector3(PlayerPrefs.GetFloat("v3_x"), PlayerPrefs.GetFloat("v3_y"), vertex1.localPosition.z);
        vertex4.localPosition = new Vector3(PlayerPrefs.GetFloat("v4_x"), PlayerPrefs.GetFloat("v4_y"), vertex1.localPosition.z);
        vertex1.GetComponent<DraggableVertex>().hasChanged = true;
        vertex2.GetComponent<DraggableVertex>().hasChanged = true;
        vertex3.GetComponent<DraggableVertex>().hasChanged = true;
        vertex4.GetComponent<DraggableVertex>().hasChanged = true;
    }
}
