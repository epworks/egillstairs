﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionFloor : MonoBehaviour
{

    [SerializeField]
    bool topFloor = true;
    // Use this for initialization
    void Start()
    {
        StartCoroutine(delayedRotate(0.1f));
    }

    // Update is called once per frame
    void Update()
    {
        // transform.LookAt(new Vector3(transform.position.x,0,0), Vector3.right);

    }
    IEnumerator delayedRotate(float delayTime)
    {
        yield return new WaitForSeconds(delayTime);
        StairGenerator stairGen = GameObject.Find("StaircaseGenerator").GetComponent<StairGenerator>();
        float stairWidth = stairGen.stairWidth;
        float stairDepth = stairGen.stairDepth;
        float stairHeight = stairGen.stairHeight;
        Transform stairs = GameObject.Find("Stairs").transform;
        Vector3 center = stairGen.absoluteCenter;
        Transform bottomStair = stairs.transform.GetChild(0).transform;
        Transform topStair = stairs.transform.GetChild(stairs.transform.childCount - 1).transform;
        float floorDepth = stairDepth*2+0.01f;
        float scalar = 0.1f;
        if(transform.name=="BottomFloor")
        scalar = 0.3f;
        transform.localScale = new Vector3(stairWidth*0.1f, 1, floorDepth*scalar);
        if(topFloor)
        transform.position = new Vector3(topStair.position.x, topStair.position.y+stairHeight/2 -0.075f, topStair.position.z + floorDepth - (floorDepth/8));
        else
        transform.position = new Vector3(topStair.position.x, bottomStair.position.y-stairHeight/2, bottomStair.position.z - floorDepth + (floorDepth/8));
    }
}
