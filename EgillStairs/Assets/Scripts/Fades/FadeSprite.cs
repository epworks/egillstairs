﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeSprite : MonoBehaviour
{

    public bool start = false;
    SpriteRenderer sprite;
    // Use this for initialization
    void Start()
    {
        sprite = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {

        if (start)
        {
            StartCoroutine(fadeOut());
            start = false;
        }

    }

    public void startFadeIn()
    {
        Debug.Log(transform.name + " fade IN ");
        StartCoroutine(fadeIn());

    }

    public void startDelayedFadeIn(float delayTime)
    {
        StartCoroutine(DelayedFadeIn(delayTime));

    }

    public void startFadeInFadeOut()
    {
        StartCoroutine(fadeInFadeOut());

    }

    public void startFadeOut()
    {
        Debug.Log(transform.name + " fade OUT ");

        StartCoroutine(fadeOut());

    }

    public void SetInvisible()
    {
        Color spriteColor = sprite.color;
        sprite.color = new Color(1, 1, 1, 0);
    }

    public void SetVisible()
    {
        Color spriteColor = sprite.color;
        sprite.color = new Color(1, 1, 1, 1);
    }

    IEnumerator DelayedFadeIn(float delayTime)
    {
        yield return new WaitForSeconds(delayTime);

        Color btColor = sprite.color;
        for (float f = 1.0f; f >= 0f; f -= 0.05f)
        {
            sprite.color = new Color(btColor.r, btColor.g, btColor.g, f);
            yield return new WaitForSeconds(0.01f);
        }
    }


    IEnumerator fadeIn()
    {
        Color btColor = sprite.color;
        for (float f = 0.0f; f <= 1.0f; f += 0.05f)
        {
            sprite.color = new Color(btColor.r, btColor.g, btColor.g, f);
            yield return new WaitForSeconds(0.01f);
        }
    }

    IEnumerator fadeInFadeOut()
    {
        Color btColor = sprite.color;
        for (float f = 1.0f; f >= 0f; f -= 0.05f)
        {
            sprite.color = new Color(btColor.r, btColor.g, btColor.g, f);
            yield return new WaitForSeconds(0.025f);
        }
        yield return new WaitForSeconds(5f);

        yield return StartCoroutine(fadeOut());
    }

    public IEnumerator fadeOut()
    {
        Color btColor = sprite.color;
        for (float f = 1.0f; f >= 0f; f -= 0.05f)
        {
            sprite.color = new Color(btColor.r, btColor.g, btColor.g, f);
            yield return new WaitForSeconds(0.025f);
        }
        SetInvisible(); //just to make sure its invisible
    }


}
