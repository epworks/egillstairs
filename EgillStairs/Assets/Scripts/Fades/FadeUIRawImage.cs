﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeUIRawImage : MonoBehaviour
{

    public bool start = false;
    RawImage image;
    bool delay = true;
    // Use this for initialization
    void Awake()
    {
        image = transform.GetComponent<RawImage>();
        if (image.color == null)
        {
            Debug.Log(transform.parent.name + " has no image !");
        }
    }

    // Update is called once per frame
    void Update()
    {

        if (start)
        {
            StartCoroutine(FadeOut(0.5f));
            start = false;
        }

    }

    public void startFadeIn(float fadeTime)
    {
        StartCoroutine(FadeIn(fadeTime));

    }

    public void startDelayedFadeIn(float delayTime = 1f, float fadeTime = 0.5f)
    {
        StartCoroutine(DelayedFadeIn(delayTime, fadeTime));

    }
    public void startDelayedFadeOut(float delayTime = 1f, float fadeTime = 0.5f)
    {
        StartCoroutine(DelayedFadeOut(delayTime, fadeTime));

    }
    public void startDelayedCustomFade(Color colorStart, Color colorEnd, float delayTime = 1f, float fadeTime = 0.5f, bool setInvisible=false)
    {
        StartCoroutine(DelayedCustomFade(colorStart, colorEnd, delayTime, fadeTime, setInvisible));

    }

    public void startCustomFadeInFadeOut(Color startColor, Color endColor, float delayTime = 1f, float fadeTime = 0.5f)
    {
        StartCoroutine(CustomFadeInFadeOut(startColor, endColor, delayTime, fadeTime));

    }

    public void startCustomFade(Color startColor, Color endColor, float fadeTime = 0.5f, bool setInvisible = false)
    {
        StartCoroutine(CustomFade(startColor, endColor, fadeTime, setInvisible));

    }

    public void SetInvisible()
    {
        Color c = image.color;
        c.a = 0;
        image.color = c;
    }
    public void SetVisible()
    {
        Color c = image.color;
        c.a = 1;
        image.color = c;
    }

    public void startFadeOut(float duration)
    {
        StartCoroutine(FadeOut(duration));

    }

    IEnumerator DelayedFadeIn(float delayTime, float fadeTime)
    {
        Color c = image.color;
        Color start = new Color(c.r, c.g, c.b, c.a);
        Color end = new Color(c.r, c.g, c.b, 1);
        yield return new WaitForSeconds(delayTime);
        StartCoroutine(FadeIn(fadeTime));
    }
    IEnumerator DelayedFadeOut(float delayTime, float fadeTime)
    {
        Color c = image.color;
        Color start = new Color(c.r, c.g, c.b, c.a);
        Color end = new Color(c.r, c.g, c.b, 1);
        yield return new WaitForSeconds(delayTime);
        StartCoroutine(FadeOut(fadeTime));
    }

    IEnumerator FadeIn(float delayTime)
    {
        Color c = image.color;
        Color start = new Color(c.r, c.g, c.b, c.a);
        Color end = new Color(c.r, c.g, c.b, 1);
        for (float t = 0f; t < delayTime; t += Time.deltaTime)
        {
            float normalizedTime = t / delayTime;
            image.color = Color.Lerp(start, end, normalizedTime);
            yield return null;
        }
        SetVisible();
    }

    IEnumerator fadeInFadeOut(float delayTime = 1f, float fadeTime = 0.5f)
    {
        Color c = image.color;
        Color start = new Color(c.r, c.g, c.b, c.a);
        Color end = new Color(c.r, c.g, c.b, 1);

        for (float t = 0f; t < fadeTime; t += Time.deltaTime)
        {
            float normalizedTime = t / fadeTime;
            image.color = Color.Lerp(start, end, normalizedTime);
            yield return null;
        }
        yield return new WaitForSeconds(delayTime);
        yield return StartCoroutine(FadeOut(fadeTime));
    }



    IEnumerator FadeOut(float fadeTime)
    {

        Color c = image.color;
        Color start = new Color(c.r, c.g, c.b, c.a);
        Color end = new Color(c.r, c.g, c.b, 0);
        for (float t = 0f; t < fadeTime; t += Time.deltaTime)
        {
            float normalizedTime = t / fadeTime;
            image.color = Color.Lerp(start, end, normalizedTime);
            yield return null;
        }
        SetInvisible();
    }
IEnumerator DelayedCustomFade(Color colorStart, Color colorEnd, float delayTime, float fadeTime, bool setInvisible=false)
    {
        Color c = image.color;
        Color start = new Color(c.r, c.g, c.b, c.a);
        Color end = new Color(c.r, c.g, c.b, 1);
        yield return new WaitForSeconds(delayTime);
        StartCoroutine(CustomFade(colorStart, colorEnd, fadeTime, setInvisible));
    }

    IEnumerator CustomFadeInFadeOut(Color start, Color end, float delayTime = 1f, float fadeTime = 0.5f, bool setInvisible = false)
    {
        for (float t = 0f; t < fadeTime; t += Time.deltaTime)
        {
            float normalizedTime = t / fadeTime;
            image.color = Color.Lerp(start, end, normalizedTime);
            yield return null;
        }
        yield return new WaitForSeconds(delayTime);
        //flip the start and end colors to fade out again
        yield return StartCoroutine(CustomFade(end, start, fadeTime, setInvisible));
    }


    IEnumerator CustomFade(Color start, Color end, float fadeTime, bool setInvisible = false)
    {
        Debug.Log("custom fade");
        for (float t = 0f; t < fadeTime; t += Time.deltaTime)
        {
            float normalizedTime = t / fadeTime;
            image.color = Color.Lerp(start, end, normalizedTime);
            yield return null;
        }
        if (setInvisible)
            SetInvisible();
    }



}
