﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeImage : MonoBehaviour
{

    public bool start = false;
    Material m_Material;
    bool delay = true;
    // Use this for initialization
    void Start()
    {
        m_Material = GetComponent<Renderer>().material;
    }

    // Update is called once per frame
    void Update()
    {

        if (start)
        {
            StartCoroutine(fadeOut());
            start = false;
        }

    }

    public void startFadeIn()
    {
        StartCoroutine(fadeIn());

    }

    public void startDelayedFadeIn(float delayTime)
    {
        StartCoroutine(DelayedFadeIn(delayTime));

    }

    public void startFadeInFadeOut()
    {
        StartCoroutine(fadeInFadeOut());

    }

    public void startFadeOut()
    {
        StartCoroutine(fadeOut());

    }

    public void SetInvisible()
    {
        Image img = GetComponent<Image>();
        Color imgColor = img.color;
        img.color = new Color(1, 1, 1, 0);
    }

    public void SetVisible()
    {
        Image img = GetComponent<Image>();
        Color imgColor = img.color;
        img.color = new Color(1, 1, 1, 1);
    }

    IEnumerator DelayedFadeIn(float delayTime)
    {
        yield return new WaitForSeconds(delayTime);

        Image bt = GetComponent<Image>();
        Color btColor = bt.color;
        for (float f = 1.0f; f >= 0f; f -= 0.05f)
        {
            bt.color = new Color(btColor.r, btColor.g, btColor.g, f);
            yield return new WaitForSeconds(0.025f);
        }
    }


    IEnumerator fadeIn()
    {
        Image bt = GetComponent<Image>();
        Color btColor = bt.color;
        for (float f = 1.0f; f >= 0f; f -= 0.05f)
        {
            bt.color = new Color(btColor.r, btColor.g, btColor.g, f);
            yield return new WaitForSeconds(0.025f);
        }
    }

    IEnumerator fadeInFadeOut()
    {
        Image bt = GetComponent<Image>();
        Color btColor = bt.color;
        for (float f = 0.0f; f <= 1f; f += 0.05f)
        {
            bt.color = new Color(btColor.r, btColor.g, btColor.g, f);
            yield return new WaitForSeconds(0.025f);
        }
        yield return new WaitForSeconds(5f);

        yield return StartCoroutine(fadeOut());
    }

    public IEnumerator fadeOut()
    {
        Image bt = GetComponent<Image>();
        Color btColor = bt.color;
        for (float f = 1.0f; f >= 0f; f -= 0.05f)
        {
            bt.color = new Color(btColor.r, btColor.g, btColor.g, f);
            yield return new WaitForSeconds(0.025f);
        }
        SetInvisible(); //just to make sure its invisible
    }


}
