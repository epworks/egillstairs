﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class FadeText : MonoBehaviour
{
    TextMeshProUGUI text;
    void Awake()
    {

        text = transform.GetComponent<TextMeshProUGUI>();

    }

    public void SetInvisible()
    {
        Color c = text.color;
        c.a = 0;
        text.color = c;
    }
    public void SetVisible()
    {
        Color c = text.color;
        c.a = 1;
        text.color = c;
    }
    public void startFadeIn(float fadeTime)
    {
        StartCoroutine(FadeIn(fadeTime));

    }


    public void startFadeOut(float fadeTime)
    {
        StartCoroutine(FadeOut(fadeTime));

    }

    public void startDelayedFadeIn(float delayTime = 1f, float fadeTime = 0.5f)
    {
        StartCoroutine(DelayedFadeIn(delayTime, fadeTime));

    }


    public void startDelayedFadeOut(float delayTime = 1f, float fadeTime = 0.5f)
    {
        StartCoroutine(DelayedFadeOut(delayTime, fadeTime));

    }

    public void startCustomFadeInFadeOut(Color startColor, Color endColor, float delayTime = 1f, float fadeTime = 0.5f)
    {
        StartCoroutine(CustomFadeInFadeOut(startColor, endColor, delayTime, fadeTime));

    }

    public void startCustomFade(Color startColor, Color endColor, float fadeTime = 0.5f, bool setInvisible = false)
    {
        StartCoroutine(CustomFade(startColor, endColor, fadeTime, setInvisible));

    }


    public void setInvisible(bool deactivate = false)
    {
        Color invisibleColor = new Color(1, 1, 1, 0);
        if (text != null)
            text.color = invisibleColor;
        if (deactivate)
        {
            transform.gameObject.SetActive(false);
        }
    }

    public void setVisible(bool deactivate = false)
    {
        Color visibleColor = new Vector4(1f, 1f, 1f, 1f);
        if (text != null)
            text.color = visibleColor;
    }

    // Update is called once per frame
    void Update()
    {


    }
    IEnumerator DelayedFadeIn(float delayTime, float fadeTime)
    {
        Color c = text.color;
        Color start = new Color(c.r, c.g, c.b, 0);
        Color end = new Color(c.r, c.g, c.b, 1);
        yield return new WaitForSeconds(delayTime);
        StartCoroutine(FadeIn(fadeTime));
    }

    IEnumerator DelayedFadeOut(float delayTime, float fadeTime)
    {
        Color c = text.color;
        Color start = new Color(c.r, c.g, c.b, 0);
        Color end = new Color(c.r, c.g, c.b, 1);
        yield return new WaitForSeconds(delayTime);
        StartCoroutine(FadeOut(fadeTime));
    }




    IEnumerator FadeIn(float delayTime)
    {
        Color c = text.color;
        Color start = new Color(c.r, c.g, c.b, 0);
        Color end = new Color(c.r, c.g, c.b, 1);
        for (float t = 0f; t < delayTime; t += Time.deltaTime)
        {
            float normalizedTime = t / delayTime;
            text.color = Color.Lerp(start, end, normalizedTime);
            yield return null;
        }
        SetVisible();
    }
    IEnumerator FadeOut(float fadeTime)
    {

        Color c = text.color;
        Color start = new Color(c.r, c.g, c.b, 1);
        Color end = new Color(c.r, c.g, c.b, 0);
        for (float t = 0f; t < fadeTime; t += Time.deltaTime)
        {
            float normalizedTime = t / fadeTime;
            text.color = Color.Lerp(start, end, normalizedTime);
            yield return null;
        }
        SetInvisible();
    }



    IEnumerator CustomFadeInFadeOut(Color start, Color end, float delayTime = 1f, float fadeTime = 0.5f, bool setInvisible = false)
    {
        for (float t = 0f; t < fadeTime; t += Time.deltaTime)
        {
            float normalizedTime = t / fadeTime;
            text.color = Color.Lerp(start, end, normalizedTime);
            yield return null;
        }
        yield return new WaitForSeconds(delayTime);
        //flip the start and end colors to fade out again
        yield return StartCoroutine(CustomFade(end, start, fadeTime, setInvisible));
    }


    IEnumerator CustomFade(Color start, Color end, float fadeTime, bool setInvisible = false)
    {
        Debug.Log("custom fade");
        for (float t = 0f; t < fadeTime; t += Time.deltaTime)
        {
            float normalizedTime = t / fadeTime;
            text.color = Color.Lerp(start, end, normalizedTime);
            yield return null;
        }
        if (setInvisible)
            SetInvisible();
    }
}
