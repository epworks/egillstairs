﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeObject : MonoBehaviour
{

    public bool start = false;
    Material m_Material;
    bool delay = true;
    // Use this for initialization
    void Awake()
    {
		m_Material = transform.GetComponent<Renderer>().material;
        if(m_Material.color == null){
            Debug.Log(transform.parent.name + " has no marker material !");
        }
    }

    // Update is called once per frame
    void Update()
    {

        if (start)
        {
			StartCoroutine(FadeOut(0.5f));
            start = false;
        }

    }

    public void startFadeIn(float duration)
    {
        StartCoroutine(FadeIn(duration));

    }

    public void startDelayedFadeIn(float delayTime =1f, float fadeTime=0.5f)
    {
        StartCoroutine(DelayedFadeIn(delayTime));

    }

	public void startFadeInFadeOut(float delayTime = 1f, float fadeTime = 0.5f)
    {
        Debug.Log("startFadeInFadeOut");
        StartCoroutine(fadeInFadeOut(delayTime, fadeTime));

    }
    
	public void startCustomFadeInFadeOut(Color startColor, Color endColor, float delayTime = 1f, float fadeTime = 0.5f)
    {
        StartCoroutine(CustomFadeInFadeOut(startColor, endColor, delayTime, fadeTime));
        
    }

	public void startCustomFade(Color startColor, Color endColor, float fadeTime = 0.5f, bool setInvisible=false)
    {
        StartCoroutine(CustomFade(startColor, endColor, fadeTime, setInvisible));

    }

    public void SetInvisible()
    {
        Color c = m_Material.color;
        c.a = 0;
        m_Material.color = c;
    }
    public void SetVisible()
    {
        Color c = m_Material.color;
        c.a = 1;
        m_Material.color = c;
    }
    
    public void startFadeOut(float duration)
    {
        StartCoroutine(FadeOut(duration));

    }

    IEnumerator DelayedFadeIn(float delayTime)
    {
		Color start = new Color(1, 1, 1, 0);
        Color end = new Color(1, 1, 1, 1);
        yield return new WaitForSeconds(delayTime);
		for (float t = 0f; t < delayTime; t += Time.deltaTime)
        {
			float normalizedTime = t / delayTime;
            m_Material.color = Color.Lerp(start, end, normalizedTime);
            yield return null;
        }
        		SetVisible();
    }

	IEnumerator FadeIn(float delayTime)
	{
		Color start = new Color(1,1,1,0);
		Color end = new Color(1,1,1,1);
		for (float t = 0f; t < delayTime; t += Time.deltaTime)
		{
			float normalizedTime = t / delayTime;
			m_Material.color = Color.Lerp(start, end, normalizedTime);
			yield return null;
		}
		SetVisible();
	}

	IEnumerator fadeInFadeOut(float delayTime = 1f, float fadeTime = 0.5f)
    {
        Color start = new Color(1, 1, 1, 0);
        Color end = new Color(1, 1, 1, 1);

        for (float t = 0f; t < fadeTime; t += Time.deltaTime)
        {
            float normalizedTime = t / fadeTime;
            m_Material.color = Color.Lerp(start, end, normalizedTime);
            yield return null;
        }
        yield return new WaitForSeconds(delayTime);
        yield return StartCoroutine(FadeOut(fadeTime));
    }



	IEnumerator FadeOut(float fadeTime)
    {

        Color start = new Color(1, 1, 1, 1);
        Color end = new Color(1, 1, 1, 0);
        for (float t = 0f; t < fadeTime; t += Time.deltaTime)
        {
            float normalizedTime = t / fadeTime;
            m_Material.color = Color.Lerp(start, end, normalizedTime);
            yield return null;
        }
        SetInvisible();
    }

	IEnumerator CustomFadeInFadeOut(Color start, Color end, float delayTime = 1f, float fadeTime = 0.5f, bool setInvisible = false)
    {
        for (float t = 0f; t < fadeTime; t += Time.deltaTime)
        {
            float normalizedTime = t / fadeTime;
            m_Material.color = Color.Lerp(start, end, normalizedTime);
            yield return null;
        }
        yield return new WaitForSeconds(delayTime);
        //flip the start and end colors to fade out again
        yield return StartCoroutine(CustomFade(end, start, fadeTime, setInvisible));
    }
    

	IEnumerator CustomFade(Color start, Color end, float fadeTime, bool setInvisible = false)
    {
		Debug.Log("custom fade");
        for (float t = 0f; t < fadeTime; t += Time.deltaTime)
        {
            float normalizedTime = t / fadeTime;
            m_Material.color = Color.Lerp(start, end, normalizedTime);
            yield return null;
        }
        if(setInvisible)
        SetInvisible();
    }



}
