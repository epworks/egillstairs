﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class FadeButton : MonoBehaviour
{

    public bool begin = false;
    bool isVisible = false;
    TextMeshProUGUI text;
    Image bt;
    void Start()
    {
        bt = GetComponent<Image>();
        if (transform.Find("Text") != null)
        {
            text = transform.Find("Text").GetComponent<TextMeshProUGUI>();
            //text.color = new Color32(255,255,255,255);
        }
    }

    public void onButtonPress()
    {
        Debug.Log("onButtonPress");
        if (isVisible)
        {
            startFadeOut();
        }
        else
        {
            startFadeIn();
        }
    }

    public void setVisible(bool fadeImage = true)
    {
        Color visibleColor = new Color(1, 1, 1, 1);
        if(fadeImage)
        bt.color = visibleColor;
        if (text != null)
            text.color = visibleColor;

    }

    public void setInvisible(bool deactivate = false, bool fadeImage = true)
    {
        Color invisibleColor = new Color(1, 1, 1, 0);
        if(fadeImage)
        bt.color = invisibleColor;
        if (text != null)
            text.color = invisibleColor;
        if(deactivate){
            transform.gameObject.SetActive(false);
        }
    }

    public void startFadeIn(bool fadeImage = true)
    {
        Debug.Log("startFadeIn");
        isVisible = true;
        StartCoroutine(fadeIn(fadeImage));
    }

    public void startFadeOut(bool deactivate=false, bool fadeImage = true)
    {
        Debug.Log("startFadeOut");
        isVisible = false;
        StartCoroutine(fadeOut(deactivate, fadeImage));
    }

    public void startDelayedFadeIn(float delayTime)
    {
        Debug.Log("startDelayedFadeIn");
        isVisible = true;
        StartCoroutine(DelayedFadeIn(delayTime));
    }

    public void startDelayedFadeOut(float delayTime)
    {
        Debug.Log("startDelayedFadeOut");
        isVisible = false;
        StartCoroutine(DelayedFadeOut(delayTime));
    }
    // Update is called once per frame
    void Update()
    {


    }
    IEnumerator DelayedFadeIn(float delayTime)
    {
        yield return new WaitForSeconds(delayTime);
        Color textColor = new Color(1, 1, 1, 1);
        Color btColor = bt.color;
        if (text != null)
            textColor = text.color;
        for (float f = 0.0f; f <= 1.0f; f += 0.05f)
        {
            bt.color = new Color(btColor.r, btColor.g, btColor.g, f);
            if (text != null)
                text.color = new Color(textColor.r, textColor.g, textColor.g, f );
            yield return null;
        }
        //yield return new WaitForSeconds(5f);
        //startFadeOut();
    }

    IEnumerator DelayedFadeOut(float delayTime)
    {
        yield return new WaitForSeconds(delayTime);
        Color textColor = new Color(1, 1, 1, 1);
        Color btColor = bt.color;
        if (text != null)
            textColor = text.color;
        for (float f = 1.0f; f >= 0f; f -= 0.05f)
        {
            bt.color = new Color(btColor.r, btColor.g, btColor.g, f);
            if (text != null)
                text.color = new Color(textColor.r, textColor.g, textColor.g, f);
            Debug.Log(bt.color.a.ToString());
            yield return null;
        }
    }

    IEnumerator fadeIn(bool fadeImage = true)
    {
        
        Color textColor = new Color(1, 1, 1, 1);
        Color btColor = bt.color;
        if (text != null)
            textColor = text.color;
        for (float f = 0.0f; f <= 1.0f; f += 0.05f)
        {
            if(fadeImage)
            bt.color = new Color(btColor.r, btColor.g, btColor.g, f);
            if (text != null)
                text.color = new Color(textColor.r, textColor.g, textColor.g, f);
            yield return null;
        }
        setVisible(fadeImage);
        //yield return new WaitForSeconds(5f);
        //startFadeOut();
    }

    IEnumerator fadeOut(bool deactivate = false, bool fadeImage = true)
    {
        Debug.Log("text.name: " + text.name);
        Color textColor = new Color(1, 1, 1, 1);
        Color btColor = bt.color;
        if (text != null)
            textColor = text.color;
        for (float f = 1.0f; f >= 0f; f -= 0.05f)
        {
            if(fadeImage)
            bt.color = new Color(btColor.r, btColor.g, btColor.g, f);
            if (text != null)
                text.color = new Color(textColor.r, textColor.g, textColor.g, f);
            if (isVisible)
            { // here if status changes (ie fade-in is activated again) stop fading out loop
                Debug.Log("is visible");
                break;
            }
            yield return null;
        }
        setInvisible(deactivate, fadeImage);
    }
}
