﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateLight : MonoBehaviour {

	// Use this for initialization
	[SerializeField]
	float rotateTime = 90;
	Vector3 startRot;
	Vector3 targetRot;
	void Start () {
		startRot = transform.eulerAngles;
		targetRot = new Vector3(startRot.x, startRot.y*2, startRot.z);
		StartCoroutine(spinLight(rotateTime));
	}
	
	// Update is called once per frame
	void Update () {
		// transform.eulerAngles += new Vector3(0, 0.5f,0);
	}

	IEnumerator spinLight(float revolutionTime){

		        for (float t = 0f; t < revolutionTime; t += Time.deltaTime)
        {
            float normalizedTime = t / revolutionTime;
            transform.eulerAngles = Vector3.Lerp(startRot, targetRot, normalizedTime);
            // rb.MovePosition(Vector3.Lerp(startPos, endPos, normalizedTime));
            yield return null;
        }
		transform.eulerAngles = targetRot;
		startRot = transform.eulerAngles;
		targetRot = startRot*2;
		StartCoroutine(spinLight(60));

	}
}
