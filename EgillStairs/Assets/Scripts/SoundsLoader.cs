﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class SoundsLoader : MonoBehaviour
{

    [SerializeField]
    string instrumentFolderName;
    // Use this for initialization
    string path;
    private Object[] files;
    void Start()
    {
        // path = "Assets/Resources/SoundBank/" + instrumentFolderName;
        string resourcePath = "SoundBank1/" + instrumentFolderName;
        files = Resources.LoadAll(resourcePath, typeof(AudioClip));

        foreach (var f in files)
        {
            string soundPath = resourcePath + "/" + f.name;
            AudioClip clip = Resources.Load<AudioClip>(soundPath);
            AudioSource audiosrc = transform.gameObject.AddComponent<AudioSource>() as AudioSource;
            audiosrc.bypassEffects = true;
            audiosrc.bypassListenerEffects = true;
            audiosrc.bypassReverbZones = true;
            audiosrc.playOnAwake = false;
            audiosrc.clip = clip;
        }

        //  AudioClip clip2 = Resources.Load<AudioClip>(path);

    }

    // Update is called once per frame
    void Update()
    {

    }
}
