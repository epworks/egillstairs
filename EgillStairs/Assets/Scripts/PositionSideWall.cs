﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionSideWall : MonoBehaviour
{

    [SerializeField]
    bool leftSide = true;
	float wallSide;
    // Use this for initialization
    void Start()
    {
        StartCoroutine(delayedRotate(0.1f));
        if (leftSide)
            wallSide = -1;
        else
            wallSide = 1;
    }

    // Update is called once per frame
    void Update()
    {
        // transform.LookAt(new Vector3(transform.position.x,0,0), Vector3.right);

    }
    IEnumerator delayedRotate(float delayTime)
    {
        yield return new WaitForSeconds(delayTime);
        StairGenerator stairGen = GameObject.Find("StaircaseGenerator").GetComponent<StairGenerator>();
        float stairWidth = stairGen.stairWidth;
        Transform stairs = GameObject.Find("Stairs").transform;
        Vector3 center = stairGen.absoluteCenter;
        Transform bottomStair = stairs.transform.GetChild(0).transform;
        Transform topStair = stairs.transform.GetChild(stairs.transform.childCount - 1).transform;
        float staircaseLength = Vector3.Distance(bottomStair.position, topStair.position);

        transform.localScale = new Vector3(0.25f, 1, 0.15f+staircaseLength * 0.15f);
        GameObject dummyNode = new GameObject();
        dummyNode.transform.position = bottomStair.position;
        dummyNode.transform.LookAt(topStair.position, Vector3.up);
        transform.position = new Vector3(center.x + stairWidth / 2 * wallSide, center.y, center.z);
        transform.eulerAngles = new Vector3(dummyNode.transform.eulerAngles.x * -1, 180, 90 * -wallSide);
        if(!leftSide)
            transform.eulerAngles += new Vector3(-180,0,0);

    }
}
