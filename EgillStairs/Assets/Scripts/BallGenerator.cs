﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.IO;

public struct BallID
{
    public GameObject ball;
    public int setIndex;
    public int typeIndex;
}
public class BallGenerator : MonoBehaviour
{
    int setChangeInterval;
    int chooseBallAttempts;
    string path;
    private Object[] files;

    public int ballSetIndex = 2;
    int minBallRange = 1;
    int maxBallRange = 5;

    [SerializeField]
    int numActiveBalls = 2;
    private List<GameObject> ballSet01 = new List<GameObject>();
    private List<GameObject> ballSet02 = new List<GameObject>();
    private List<GameObject> ballSet03 = new List<GameObject>();
    private bool destroyBall = false;
    private bool addNewBall = false;
    List<BallID> activeBalls = new List<BallID>();
    int ballTypeIndex = 0;
    bool newMinute = false;
    int totalBallCount = 0;
    int numBallModeIndex = 0;
    int timeInterval = 60; // interval between changing number of balls on stairs
    bool newBallset = false;

    List<string> ballTypeList = new List<string>();
    TextMeshProUGUI numBallsDisplay;
    TextMeshProUGUI timerDisplay;
    // Use this for initialization
    void Start()
    {
            Cursor.visible = false;

        generateBallSets(ballSet01, "Set01");
        generateBallSets(ballSet02, "Set02");
        generateBallSets(ballSet03, "Set03");

        setChangeInterval = Random.Range(1, 5);

        numBallsDisplay = GameObject.Find("NumBalls").GetComponent<TextMeshProUGUI>();
        timerDisplay = GameObject.Find("Timer").GetComponent<TextMeshProUGUI>();
        QualitySettings.antiAliasing = 2;

        Physics.gravity = new Vector3(0, -9.8F * 0.5f, 0); //default -9.8F

        StartCoroutine(changeBallset());

    }

    void generateBallSets(List<GameObject> ballset, string prefabSetName)
    {
        files = Resources.LoadAll(prefabSetName, typeof(GameObject));
        foreach (var f in files)
        {
            string prefabPath = prefabSetName + "/" + f.name;
            GameObject ball = Resources.Load<GameObject>(prefabPath);
            ballset.Add(ball);
        }
    }

    void addBall()
    {
        chooseBallAttempts = 0;
        //decide what prefab ball to generate
        getBallCount(); // manage number of balls in the scene

        GameObject newBall = getBallFromSet(ballTypeIndex, ballSetIndex);
        BallID newId = new BallID();
        newId.ball = Instantiate(newBall, newBall.transform.position, Quaternion.identity, transform);
        newId.setIndex = ballSetIndex;
        newId.typeIndex = ballTypeIndex;
        activeBalls.Add(newId);
    }

    void getBallId()
    {
        chooseBallAttempts++;
        int ran = UnityEngine.Random.Range(0, 100);
        int currentSetSize = getSetFromIndex(ballSetIndex).Count;
        Debug.Log("set: " + ballSetIndex + " currentSetSize: " + currentSetSize);
        int equalDivision = 100 / currentSetSize; // equal chance of getting any ball in set
        for (int i = 0; i < equalDivision; i++)
        {
            if (ran < equalDivision * i)
            {
                //make sure its with range of list size
                ballTypeIndex = (int)Mathf.Min(Mathf.Max(0, i), currentSetSize);
                break;
            }
        }

    }
    void getBallCount()
    {
        getBallId();
        int maxBalls = Random.Range(minBallRange, maxBallRange);
        if (ballSetIndex == 0)
        {
            minBallRange = 1;
            maxBallRange = 5;
        }
        else if (ballSetIndex == 1)
        {
            minBallRange = 1;
            maxBallRange = 5;
        }
        else
        {
            minBallRange = 1;
            maxBallRange = 3;
        }
        int totalBalls = 0;
        bool withinRange = false; //is chosen ball within range of acceptable ball type limit?
        while (!withinRange)
        {
            foreach (BallID ballId in activeBalls)
            {
                if (ballId.setIndex == ballSetIndex && ballId.typeIndex == ballTypeIndex)
                {
                    totalBalls++;
                }
            }
            if (totalBalls < maxBalls || chooseBallAttempts > 20)
                withinRange = true;
            else
                getBallId(); // if not within range, get new ball type
        }
        if (chooseBallAttempts > 20)
        { // if maxxed out, don't try to add any more balls
            totalBallCount = activeBalls.Count;
            numActiveBalls = totalBallCount;
        }
    }

    GameObject getBallFromSet(int typeIndex, int setIndex)
    {
        List<GameObject> ballset;
        switch (setIndex)
        {
            default:
                ballset = ballSet01;
                break;
            case 1:
                ballset = ballSet02;
                break;
            case 2:
                ballset = ballSet03;
                break;
        }

        // Debug.Log("ballSetIndex: " + ballSetIndex + " typeIndex: " + typeIndex + " ballset.Count : " + ballset.Count);
        // Debug.Log("ballset[typeIndex]: " + ballset[typeIndex].name);
        return ballset[typeIndex - 1];
    }

    List<GameObject> getSetFromIndex(int setIndex)
    {
        List<GameObject> ballset;
        switch (setIndex)
        {
            default:
                ballset = ballSet01;
                break;
            case 1:
                ballset = ballSet02;
                break;
            case 2:
                ballset = ballSet03;
                break;
        }
        return ballset;
    }

    // Update is called once per frame
    void Update()
    {
        int curTime = (int)Time.time;
        ManageBalls();

        if (destroyBall)
            manualDestroyBall();

        if (addNewBall)
        {
            addBall();
            addNewBall = false;
        }

        timerDisplay.text = "Time until next change: " + ((int)(timeInterval - (Time.time % timeInterval))).ToString();
        if (curTime % timeInterval == 0)
            Debug.Log("Change ball generation state");

        if (curTime % timeInterval == 0 && !newMinute)
        {
            StartCoroutine(changeNumBalls());
        }

        if (curTime % (timeInterval * setChangeInterval) == 0 && !newBallset)
        {
            StartCoroutine(changeBallset());
            setChangeInterval = Random.Range(1, 5);
        }


        if (Input.GetKeyDown("x"))
            destroyBall = true;

        if (Input.GetKeyDown(KeyCode.Return))
            addNewBall = true;


    }

    void manualDestroyBall()
    {
        Destroy(activeBalls[activeBalls.Count - 1].ball);
        activeBalls.RemoveAt(activeBalls.Count - 1);
        addBall();
        destroyBall = false;
    }

    public void destroyAtIndex(int index)
    {
        Destroy(activeBalls[index].ball);
        activeBalls.RemoveAt(index);
        addBall();
    }

    void ManageBalls()
    {
        int ballIndex = 0;
        List<int> removeBalls = new List<int>();

        //first just add balls to be destroyed to a list
        foreach (BallID ballId in activeBalls)
        {
            if (ballId.ball.transform.position.y < -1)
            {
                removeBalls.Add(ballIndex);
            }
            ballIndex++;
        }
        //now destroy and remove from list and add new ball 
        foreach (int destroyIndex in removeBalls)
        {
            Destroy(activeBalls[destroyIndex].ball);
            activeBalls.RemoveAt(destroyIndex);
            // Debug.Log(activeBalls.Count + " // " + numActiveBalls);
            if (activeBalls.Count < numActiveBalls)
                addBall();
        }
        // Debug.Log("activeBalls.Count : " + activeBalls.Count + " numActiveBalls: " + numActiveBalls);
        if (activeBalls.Count < numActiveBalls)
            StartCoroutine(addBalls());
    }

    IEnumerator changeBallset()
    {
        int maxBalls = Random.Range(minBallRange, maxBallRange);
        newBallset = true;
        int ran = getRanNum();
        if (ran < 33)
        {
            ballSetIndex = 0;
        }
        else if (ran > 33 && ran < 66)
        {
            ballSetIndex = 1;
        }
        else if (ran > 66)
        {
            ballSetIndex = 2;
        }
        updateBallsetGui();
        yield return new WaitForSeconds(1.01f);
        newBallset = false;
    }
    IEnumerator changeNumBalls()
    {
        Debug.Log("change ball set");
        newMinute = true;
        yield return new WaitForSeconds(1.01f);
        int ran = getRanNum();
        if (ran < 40)
            totalBallCount = 1;
        else if (ran > 40 && ran < 60)
            totalBallCount = 2;
        else if (ran > 60 && ran < 80)
            totalBallCount = Random.Range(3, 5);
        else
            totalBallCount = Random.Range(5, 12);

        ran = getRanNum();
        if (ran < 25)
            timeInterval = 30;
        else if (ran > 25 && ran < 50)
            timeInterval = 60;
        else if (ran > 50 && ran < 75)
            timeInterval = 90;
        else
            timeInterval = 120;

        Debug.Log("num balls: " + totalBallCount.ToString());
        numBallsDisplay.text = "Total active balls: " + totalBallCount.ToString();

        numActiveBalls = totalBallCount;
        newMinute = false;


    }
    void updateBallsetGui()
    {
        // GameObject.Find("Toggle_Set01").GetComponent<Toggle>().isOn=true;

        Debug.Log("updateBallsetGui : " + ballSetIndex);
        if (ballSetIndex == 0)
        {
            GameObject.Find("Toggle_Set01").GetComponent<Toggle>().isOn = true;
            GameObject.Find("Toggle_Set02").GetComponent<Toggle>().isOn = false;
            GameObject.Find("Toggle_Set03").GetComponent<Toggle>().isOn = false;
        }
        else if (ballSetIndex == 1)
        {
            GameObject.Find("Toggle_Set01").GetComponent<Toggle>().isOn = false;
            GameObject.Find("Toggle_Set02").GetComponent<Toggle>().isOn = true;
            GameObject.Find("Toggle_Set03").GetComponent<Toggle>().isOn = false;
        }
        else if (ballSetIndex == 2)
        {
            GameObject.Find("Toggle_Set01").GetComponent<Toggle>().isOn = false;
            GameObject.Find("Toggle_Set02").GetComponent<Toggle>().isOn = false;
            GameObject.Find("Toggle_Set03").GetComponent<Toggle>().isOn = true;
        }
    }
    int getRanNum()
    {
        return UnityEngine.Random.Range(0, 100);
    }

    IEnumerator addBalls()
    {
        while (activeBalls.Count < numActiveBalls)
        {
            addBall();
            yield return new WaitForSeconds(1.0f);
        }

    }
}
