﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class DoubleJump : MonoBehaviour
{
    //directionality of ball -- directions relative to camera
    enum Direction { Up, Down, Left, Right, Stay };

    Direction currentDir = Direction.Down;
    float stairDepth;
    float stairHeight;
    float stairWidth;
    int numStairs;
    [SerializeField]
    float volumeLevel = 0.1f;
    [SerializeField]
    string soundBankName;

    [SerializeField]
    float thrustAmt = 0.1f;
    [SerializeField]
    int exploreMax = 6;
    [SerializeField]
    int exploreMin = 3;
    [SerializeField]
    float horizontalThrustAmt = 0.2f;
    [SerializeField]
    public float dropHeight = 0.5f;
    [SerializeField]
    bool startAtBottom = false;
    [SerializeField]
    float DownwardLikeliness = 70;
    [SerializeField]
    float StayLikeliness = 30;
    [SerializeField]
    bool explorationMode;
    [SerializeField]
    float explorationLikeliness = 25;
    int explorationDistance = 2;
    bool isExploring = false;
    int explorationCount = 0;
    bool isFalling = false;
    bool isLifting = false;
    bool targetReached = false;
    Vector3 target;
    int isStillCount = 0;

    GameObject soundBank;
    List<AudioSource> sounds = new List<AudioSource>();

    // Use this for initialization
    Rigidbody rb;
    void Start()
    {
        soundBank = GameObject.Find(soundBankName);
        foreach (AudioSource soundObject in soundBank.transform.GetComponents<AudioSource>())
        {
            soundObject.volume = volumeLevel;
            sounds.Add(soundObject);
        }

        rb = transform.GetComponent<Rigidbody>();
        Transform stairGen = GameObject.Find("StaircaseGenerator").transform;
        Transform stairs = stairGen.Find("Stairs").transform;
        numStairs = GameObject.Find("StaircaseGenerator").GetComponent<StairGenerator>().numStairs; //stairs.GetChildCount();
        stairDepth = stairGen.GetComponent<StairGenerator>().stairDepth;
        stairHeight = stairGen.GetComponent<StairGenerator>().stairHeight;
        stairWidth = stairGen.GetComponent<StairGenerator>().stairWidth;
        float randomX = UnityEngine.Random.Range(-stairWidth / 2 + (stairWidth / 8), stairWidth / 2 - (stairWidth / 8));

        Vector3 startStairPos;
        if (startAtBottom)
        {
            currentDir = Direction.Up;
            startStairPos = GameObject.Find("BottomFloor").transform.position;
            transform.position = new Vector3(startStairPos.x + randomX, startStairPos.y - dropHeight, startStairPos.z - 0.5f); // set position to first stair
            StartCoroutine(MoveToStartPos(transform.position, new Vector3(startStairPos.x + randomX, startStairPos.y + dropHeight, startStairPos.z - (stairDepth / 2))));
            // chooseNewDirection();
        }
        else
        {
            startStairPos = stairs.GetChild(stairs.childCount - 1).transform.position;
            transform.position = new Vector3(startStairPos.x + randomX, startStairPos.y + dropHeight, startStairPos.z - (stairDepth / 2)); // set position to first stair
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        isMoving();

        Vector3 localVelocity = transform.InverseTransformDirection(transform.GetComponent<Rigidbody>().velocity);
        //Debug.Log(localVelocity.y.ToString());

        // if (localVelocity.y < 0 && !isFalling)
        // {
        //     isFalling = true;
        //     isLifting = false;
        //     // Debug.Log(transform.position.ToString());
        //     // MoveToPosition(transform.position);
        //     chooseNewDirection();
        //     MoveForce(transform.position);
        // }
        // if (localVelocity.y > 0 && !isLifting)
        // {
        //     // Debug.Log("up");
        //     isFalling = false;
        //     isLifting = true;
        // }



    }

    void chooseNewDirection()
    {
        //     Array values = Enum.GetValues(typeof(Direction));
        // 	System.Random random = new System.Random();
        // Direction newDirection = (Direction)values.GetValue(random.Next(values.Length));
        Direction newDirection = currentDir;
        if (!isExploring)
        {
            if (getRanNum() <= DownwardLikeliness)
                newDirection = Direction.Down;
            else
                newDirection = Direction.Up;

            if (getRanNum() <= StayLikeliness)
                newDirection = Direction.Stay;
        }
        //if enabled, explore the stairs sometimes by going side to side
        newDirection = exploreStairs(newDirection);
        Debug.Log("currentDir: " + currentDir + " newDir: " + newDirection + " isExploring: " + isExploring.ToString());

        currentDir = newDirection;
        // Debug.Log(currentDir.ToString());
    }

    Direction exploreStairs(Direction currentDirection)
    {
        Direction newDirection = currentDirection;
        //if isExploring true, behaviour tends to go left and right for awhile
        if (isExploring)
        {
            rb.constraints = RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotationZ;
            // Debug.Log("is exploring " + currentDir.ToString() + ": " + explorationCount.ToString() + "/" + explorationDistance);
            explorationCount++;

            //if close to the wall turn off side to side movement
            float stairEdgeMargin = (stairWidth / 10);
            if (transform.position.x < -(stairWidth / 2) + stairEdgeMargin)
            {
                currentDir = Direction.Right;
                explorationMode = false;
            }
            if (transform.position.x > (stairWidth / 2) - stairEdgeMargin)
            {
                currentDir = Direction.Left;
                explorationMode = false;
            }
            //change direction every five steps
            if (explorationCount > explorationDistance)
                isExploring = false;
        }
        else
        {
            rb.constraints = RigidbodyConstraints.None; // turn off constriants
            if (getRanNum() <= explorationLikeliness)
            {
                isExploring = true;
                explorationCount = 0;
                explorationDistance = (int)UnityEngine.Random.Range(exploreMin, exploreMax);
                if (getRanNum() < 50)
                    newDirection = Direction.Left;
                else
                    newDirection = Direction.Right;
                // Debug.Log("set exploration mode to: " + newDirection);
            }

        }
        return newDirection;
    }
    int getRanNum()
    {
        return UnityEngine.Random.Range(0, 100);
    }
    void isMoving()
    {
        float speed = rb.velocity.magnitude;
        if (speed == 0.0f)
            isStillCount++;
        else
            isStillCount = 0;

        if (isStillCount > 600)
            transform.parent.GetComponent<BallGenerator>().destroyAtIndex(transform.GetSiblingIndex());

    }
    void OnCollisionExit(Collision collisionInfo)
    {

        if (collisionInfo.transform.name == "TopFloor")
        {
            sounds[numStairs].PlayOneShot(sounds[numStairs].clip, volumeLevel);
        }
        else if (collisionInfo.transform.name == "BottomFloor")
        {
            sounds[0].PlayOneShot(sounds[0].clip, volumeLevel);
        }
        else
        {
            int parsedStairNum;
            if (int.TryParse(collisionInfo.transform.name, out parsedStairNum))
                sounds[parsedStairNum + 1].PlayOneShot(sounds[parsedStairNum + 1].clip, volumeLevel);
        }

        chooseNewDirection();
        MoveForce(transform.position);

        //         print("No longer in contact with " + float.TryParse(collisionInfo.transform.name, out defaultVal));
    }

    // void OnCollisionStay(Collision collisionInfo)
    // {
    // 	Debug.Log("STAY");
    // }
    void MoveForce(Vector3 startPos)
    {

        if (explorationMode)
            chooseNewDirection();
        float horizontality = 0;
        float verticality;
        float lift;
        if (currentDir == Direction.Down)
        {
            horizontality = 0;
            verticality = -1;
            lift = 0;
        }
        else if (currentDir == Direction.Up)
        {
            horizontality = 0;
            verticality = 1;
            lift = 2;
        }
        else if (currentDir == Direction.Stay)
        {
            horizontality = 0;
            verticality = 0;
            lift = 0;
            chooseNewDirection();
        }
        else
        {
            verticality = 0;
            lift = 0;
            // chooseNewDirection();
        }
        if (currentDir == Direction.Left)
        {
            horizontality = -1;
            verticality = 0;
        }
        else if (currentDir == Direction.Right)
        {
            horizontality = 1;
            verticality = 0;
        }

        rb.constraints = RigidbodyConstraints.None; // turn off constriants
        //add force to move toward target
        targetReached = false;
        target = startPos + new Vector3(0, 0, verticality * stairDepth);
        rb.AddForce(horizontality * horizontalThrustAmt, lift * thrustAmt, verticality * thrustAmt, ForceMode.Impulse);

    }

    IEnumerator MoveToStartPos(Vector3 startPos, Vector3 endPos)
    {
        float moveTime = 0.5f;
        //	        yield return new WaitForSeconds(delayTime);
        for (float t = 0f; t < moveTime; t += Time.deltaTime)
        {
            float normalizedTime = t / moveTime;
            transform.position = Vector3.Lerp(startPos, endPos, normalizedTime);
            // rb.MovePosition(Vector3.Lerp(startPos, endPos, normalizedTime));

            yield return null;
        }

        transform.position = endPos;
    }

}