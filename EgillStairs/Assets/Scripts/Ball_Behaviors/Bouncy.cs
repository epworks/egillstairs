﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Bouncy : MonoBehaviour
{
    //directionality of ball -- directions relative to camera
    enum Direction { Up, Down, Left, Right, Stay };

    Direction currentDir = Direction.Down;
    float stairDepth;
    float stairHeight;
    float stairWidth;
        [SerializeField]
    float volumeLevel = 0.1f;
    [SerializeField]
    float thrustAmt = 0.1f;
    [SerializeField]
    public float dropHeight = 0.5f;
    [SerializeField]
    bool startAtBottom = false;
    [SerializeField]
    float DownwardLikeliness = 70;
    [SerializeField]
    float StayLikeliness = 30;
    bool isFalling = false;
    bool isLifting = false;
    bool targetReached = false;
    Vector3 target;
    int isStillCount = 0;
    // Use this for initialization
    Rigidbody rb;
    AudioSource sound;
    void Start()
    {
        sound = GameObject.Find("SoundPlayer").GetComponent<AudioSource>();
        rb = transform.GetComponent<Rigidbody>();
        Transform stairGen = GameObject.Find("StaircaseGenerator").transform;
        Transform stairs = stairGen.Find("Stairs").transform;
        stairDepth = stairGen.GetComponent<StairGenerator>().stairDepth;
        stairHeight = stairGen.GetComponent<StairGenerator>().stairHeight;
        stairWidth = stairGen.GetComponent<StairGenerator>().stairWidth;
        float randomX = UnityEngine.Random.Range(-stairWidth / 2 + (stairWidth / 8), stairWidth / 2 - (stairWidth / 8));

        Vector3 startStairPos;
        if (startAtBottom)
        {
            startStairPos = stairs.GetChild(0).transform.position;
            chooseNewDirection();
        }
        else
        {
            startStairPos = stairs.GetChild(stairs.childCount - 1).transform.position;
        }
        transform.position = new Vector3(startStairPos.x + randomX, startStairPos.y + dropHeight, startStairPos.z - (stairDepth / 2)); // set position to first stair
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        isMoving();

        Vector3 localVelocity = transform.InverseTransformDirection(transform.GetComponent<Rigidbody>().velocity);
        //Debug.Log(localVelocity.y.ToString());

        if (localVelocity.y < 0 && !isFalling)
        {
            isFalling = true;
            isLifting = false;
            Debug.Log(transform.position.ToString());
            MoveToPosition(transform.position);
        }
        if (localVelocity.y > 0 && !isLifting)
        {
            // Debug.Log("up");
            isFalling = false;
            isLifting = true;
        }



    }

    void chooseNewDirection()
    {
        //     Array values = Enum.GetValues(typeof(Direction));
        // 	System.Random random = new System.Random();
        // Direction newDirection = (Direction)values.GetValue(random.Next(values.Length));

        Direction newDirection;
        int ran = UnityEngine.Random.Range(0, 100);
        if (ran <= DownwardLikeliness)
            newDirection = Direction.Down;
        else
            newDirection = Direction.Up;

        ran = UnityEngine.Random.Range(0, 100);
        if (ran <= StayLikeliness)
            newDirection = Direction.Stay;

        currentDir = newDirection;
        Debug.Log(currentDir.ToString());
    }

    void MoveToPosition(Vector3 startPos)
    {
        if (currentDir == Direction.Down)
            StartCoroutine(DownStep(startPos));
        else if (currentDir == Direction.Up)
            StartCoroutine(UpStep(startPos));
        else
            chooseNewDirection();
    }


    void isMoving()
    {
        float speed = rb.velocity.magnitude;
        if (speed == 0.0f)
            isStillCount++;
        else
            isStillCount = 0;

        if (isStillCount > 20)
            transform.parent.GetComponent<BallGenerator>().destroyAtIndex(transform.GetSiblingIndex());

    }

    IEnumerator DownStep(Vector3 startPos)
    {
        Vector3 endPos = startPos + new Vector3(0, 0, -stairDepth);
        float moveTime = 0.1f;
        //	        yield return new WaitForSeconds(delayTime);
        for (float t = 0f; t < moveTime; t += Time.deltaTime)
        {
            float normalizedTime = t / moveTime;
            // transform.position = Vector3.Lerp(startPos, endPos, normalizedTime);
            rb.MovePosition(Vector3.Lerp(startPos, endPos, normalizedTime));

            yield return null;
        }
        transform.position = endPos;

        chooseNewDirection();

    }
    IEnumerator UpStep(Vector3 startPos)
    {
        Vector3 endPos = startPos + new Vector3(0, stairHeight / 2, 0);
        float moveTime = 0.1f;
        //	        yield return new WaitForSeconds(delayTime);
        for (float t = 0f; t < moveTime; t += Time.deltaTime)
        {
            float normalizedTime = t / moveTime;
            transform.position = Vector3.Lerp(startPos, endPos, normalizedTime);
            // rb.MovePosition(Vector3.Lerp(startPos, endPos, normalizedTime));

            yield return null;
        }
        startPos = endPos;
        endPos = startPos + new Vector3(0, 0, stairDepth);
        for (float t = 0f; t < moveTime; t += Time.deltaTime)
        {
            float normalizedTime = t / moveTime;
            transform.position = Vector3.Lerp(startPos, endPos, normalizedTime);
            // rb.MovePosition(Vector3.Lerp(startPos, endPos, normalizedTime));
            yield return null;
        }
        transform.position = endPos;
        chooseNewDirection();

    }

    void OnCollisionExit(Collision collisionInfo)
    {
                if(collisionInfo.transform.name == "TopFloor")
        sound.Play();
        else if(collisionInfo.transform.name == "BottomFloor")
        sound.Play();
        else
        sound.pitch = 0.5f + (float.Parse(collisionInfo.transform.name) * 0.1f);
        sound.Play();

        //  print("No longer in contact with " + float.Parse(collisionInfo.transform.name));
    }

    // void OnCollisionStay(Collision collisionInfo)
    // {
    // 	Debug.Log("STAY");
    // }

}
