﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Diagonal : MonoBehaviour
{
    //directionality of ball -- directions relative to camera
    enum Direction { Up, Down, Left, Right, Stay };

    Direction currentDir = Direction.Down;
    Direction primaryDir = Direction.Down;
    float stairDepth;
    float stairHeight;
    float stairWidth;
    int numStairs;
    [SerializeField]
    string soundBankName;
    [SerializeField]
    float volumeLevel = 0.1f;
    [SerializeField]
    float thrustAmt = 0.1f;
    [SerializeField]
    int exploreMax = 6;
    [SerializeField]
    int exploreMin = 3;
    [SerializeField]
    float horizontalThrustAmt = 0.2f;
    [SerializeField]
    public float dropHeight = 0.5f;
    [SerializeField]
    bool startAtBottom = false;
    [SerializeField]
    float DownwardLikeliness = 70;
    [SerializeField]
    float StayLikeliness = 30;
    [SerializeField]
    bool explorationMode;
    [SerializeField]
    float explorationLikeliness = 25;
    int explorationDistance = 2;
    bool isExploring = false;
    int explorationCount = 0;
    bool isFalling = false;
    bool isLifting = false;
    bool targetReached = false;
    Vector3 target;
    int currentStairIndex = -1;
    bool counterpointMode = false;
    int isStillCount = 0;
    // Use this for initialization
    Rigidbody rb;
    GameObject soundBank;
    List<AudioSource> sounds = new List<AudioSource>();
    void Start()
    {

        if (getRanNum() < 50)
            primaryDir = Direction.Left;
        else
            primaryDir = Direction.Right;


        if (getRanNum() < 50)
        {
            startAtBottom = true;
            thrustAmt *= 2;
        }
        else
        {
            startAtBottom = false;
        }

        soundBank = GameObject.Find(soundBankName);
        foreach (AudioSource soundObject in soundBank.transform.GetComponents<AudioSource>())
        {
            soundObject.volume = volumeLevel;
            sounds.Add(soundObject);
        }
        rb = transform.GetComponent<Rigidbody>();
        Transform stairGen = GameObject.Find("StaircaseGenerator").transform;
        Transform stairs = stairGen.Find("Stairs").transform;
        numStairs = GameObject.Find("StaircaseGenerator").GetComponent<StairGenerator>().numStairs; //stairs.GetChildCount();
        stairDepth = stairGen.GetComponent<StairGenerator>().stairDepth;
        stairHeight = stairGen.GetComponent<StairGenerator>().stairHeight;
        stairWidth = stairGen.GetComponent<StairGenerator>().stairWidth;
        float randomX = UnityEngine.Random.Range(-stairWidth / 2 + (stairWidth / 8), stairWidth / 2 - (stairWidth / 8));

        Vector3 startStairPos;
        if (startAtBottom)
        {
            currentDir = Direction.Up;
            startStairPos = GameObject.Find("BottomFloor").transform.position;
            transform.position = new Vector3(startStairPos.x + randomX, startStairPos.y - dropHeight, startStairPos.z - 0.5f); // set position to first stair
            StartCoroutine(MoveToStartPos(transform.position, new Vector3(startStairPos.x + randomX, startStairPos.y + dropHeight, startStairPos.z - (stairDepth / 2))));
            // chooseNewDirection();
        }
        else
        {
            startStairPos = stairs.GetChild(stairs.childCount - 1).transform.position;
            transform.position = new Vector3(startStairPos.x + randomX, startStairPos.y + dropHeight, startStairPos.z - (stairDepth / 2)); // set position to first stair
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        isMoving();

        Vector3 localVelocity = transform.InverseTransformDirection(transform.GetComponent<Rigidbody>().velocity);
        //Debug.Log(localVelocity.y.ToString());

        // if (localVelocity.y < 0 && !isFalling)
        // {
        //     isFalling = true;
        //     isLifting = false;
        //     // Debug.Log(transform.position.ToString());
        //     // MoveToPosition(transform.position);
        //     chooseNewDirection();
        //     MoveForce(transform.position);
        // }
        // if (localVelocity.y > 0 && !isLifting)
        // {
        //     // Debug.Log("up");
        //     isFalling = false;
        //     isLifting = true;
        // }



    }

    void chooseNewDirection()
    {

        Direction newDirection = currentDir;

        //if close to the wall turn off side to side movement
        float stairEdgeMargin = (stairWidth / 10);
        if (transform.position.x < -(stairWidth / 2) + stairEdgeMargin)
        {
            primaryDir = Direction.Right;
        }
        if (transform.position.x > (stairWidth / 2) - stairEdgeMargin)
        {
            primaryDir = Direction.Left;
        }

        if (currentDir == primaryDir && !startAtBottom)
            newDirection = Direction.Down;
        else if (currentDir == primaryDir && startAtBottom)
            newDirection = Direction.Up;
        else
            newDirection = primaryDir;


        //if enabled, explore the stairs sometimes by going side to side

        currentDir = newDirection;
        // Debug.Log(currentDir.ToString());
    }

    int getRanNum()
    {
        return UnityEngine.Random.Range(0, 100);
    }
    void isMoving()
    {
        float speed = rb.velocity.magnitude;
        if (speed == 0.0f)
            isStillCount++;
        else
            isStillCount = 0;

        if (isStillCount > 600)
            transform.parent.GetComponent<BallGenerator>().destroyAtIndex(transform.GetSiblingIndex());

    }
    void OnCollisionExit(Collision collisionInfo)
    {
        // Debug.Log("name: " + numStairs);

        if (collisionInfo.transform.name == "TopFloor")
        {
            sounds[numStairs].PlayOneShot(sounds[numStairs].clip, volumeLevel);
        }
        else if (collisionInfo.transform.name == "BottomFloor")
        {
            sounds[0].PlayOneShot(sounds[0].clip, volumeLevel);
        }
        else
        {
            int parsedStairNum;
            if (int.TryParse(collisionInfo.transform.name, out parsedStairNum))
                sounds[(parsedStairNum + 1) % sounds.Count].PlayOneShot(sounds[(parsedStairNum + 1) % sounds.Count].clip, volumeLevel);
            if (parsedStairNum != currentStairIndex)
            {
                currentStairIndex = parsedStairNum;
                if (getRanNum() < 30)
                    counterpointMode = true;
            }
        }
        if (!counterpointMode)
            chooseNewDirection();
        else
            getOppositeDirection();

        MoveForce(transform.position);

    }

    void getOppositeDirection()
    {
        if (startAtBottom)
            currentDir = Direction.Down;
        else
            currentDir = Direction.Up;

    }
    // void OnCollisionStay(Collision collisionInfo)
    // {
    // 	Debug.Log("STAY");
    // }
    void MoveForce(Vector3 startPos)
    {

        float horizontality = 0;
        float verticality = 0;
        float lift = 0;
        if (currentDir == Direction.Down)
        {
            horizontality = 0;
            verticality = -1;
            lift = 0;
        }
        else if (currentDir == Direction.Up)
        {
            horizontality = 0;
            verticality = 1;
            lift = 2;
        }
        if (currentDir == Direction.Left)
        {
            horizontality = -1;
            verticality = 0;
        }
        else if (currentDir == Direction.Right)
        {
            horizontality = 1;
            verticality = 0;
        }

        rb.constraints = RigidbodyConstraints.None; // turn off constriants
        //add force to move toward target
        targetReached = false;
        target = startPos + new Vector3(0, 0, verticality * stairDepth);
        rb.AddForce(horizontality * horizontalThrustAmt, lift * thrustAmt, verticality * thrustAmt, ForceMode.Impulse);

    }
    IEnumerator MoveToStartPos(Vector3 startPos, Vector3 endPos)
    {
        float moveTime = 0.5f;
        //	        yield return new WaitForSeconds(delayTime);
        for (float t = 0f; t < moveTime; t += Time.deltaTime)
        {
            float normalizedTime = t / moveTime;
            transform.position = Vector3.Lerp(startPos, endPos, normalizedTime);
            // rb.MovePosition(Vector3.Lerp(startPos, endPos, normalizedTime));

            yield return null;
        }

        transform.position = endPos;
    }

}