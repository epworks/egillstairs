﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Snail : MonoBehaviour
{
    //directionality of ball -- directions relative to camera
    enum Direction { Up, Down, Left, Right, Stay };

    Direction currentDir = Direction.Right;
    Direction primaryDir = Direction.Right;
    int ranStair;
    float stairDepth;
    float stairHeight;
    float stairWidth;
    [SerializeField]
    float floatHeight = 0;
    [SerializeField]
    float volumeLevel = 0.1f;
    [SerializeField]
    string soundBankName;

    [SerializeField]
    float thrustAmt = 0.1f;
    [SerializeField]
    int exploreMax = 6;
    [SerializeField]
    int exploreMin = 3;
    [SerializeField]
    float horizontalThrustAmt = 0.2f;
    [SerializeField]
    public float dropHeight = 0.5f;
    [SerializeField]
    bool startAtBottom = false;
    [SerializeField]
    float DownwardLikeliness = 70;
    [SerializeField]
    float StayLikeliness = 30;
    [SerializeField]
    bool explorationMode;
    [SerializeField]
    float explorationLikeliness = 25;
    int explorationDistance = 2;
    bool isExploring = false;
    int explorationCount = 0;
    bool isFalling = false;
    bool isLifting = false;
    bool targetReached = false;
    Vector3 target;
    int isStillCount = 0;
    // Use this for initialization
    Rigidbody rb;
    GameObject soundBank;
    List<AudioSource> sounds = new List<AudioSource>();
    Vector3 startPos;
    Vector3 endPos;
    void Start()
    {

        if (getRanNum() < 50)
            primaryDir = Direction.Left;
        else
            primaryDir = Direction.Right;

        soundBank = GameObject.Find(soundBankName);
        foreach (AudioSource soundObject in soundBank.transform.GetComponents<AudioSource>())
        {
            sounds.Add(soundObject);
        }
        Transform stairGen = GameObject.Find("StaircaseGenerator").transform;
        Transform stairs = stairGen.Find("Stairs").transform;
        stairDepth = stairGen.GetComponent<StairGenerator>().stairDepth;
        stairHeight = stairGen.GetComponent<StairGenerator>().stairHeight;
        stairWidth = stairGen.GetComponent<StairGenerator>().stairWidth;
        int numStairs = GameObject.Find("StaircaseGenerator").GetComponent<StairGenerator>().numStairs; //stairs.GetChildCount();

        ranStair = UnityEngine.Random.Range(0, numStairs);
        Vector3 startStairPos = stairs.Find(ranStair.ToString()).transform.position;
        float xPos = stairWidth / 2;
        if (primaryDir == Direction.Right)
            xPos = -stairWidth / 2;
        startPos = new Vector3(xPos, (startStairPos.y + transform.localScale.y * 3) + floatHeight, startStairPos.z - (stairDepth / 2)); // set position to first stair
        endPos = new Vector3((xPos + (xPos * 0.1f)) * -1, startPos.y, startStairPos.z - (stairDepth / 2)); // set position to first stair
        transform.position = startPos;
        sounds[ranStair].volume = volumeLevel;
        sounds[ranStair].PlayOneShot(sounds[ranStair].clip, volumeLevel);

        if(transform.name=="SnailCube_S1(Clone)")
                transform.rotation = Quaternion.Euler(-90,0,0);

        float moveTime = sounds[ranStair].clip.length * 1.05f; //UnityEngine.Random.Range(2.0f, 14.0f);
        Debug.Log("startPos: " + startPos.ToString() + " endPos: " + endPos.ToString());
        StartCoroutine(moveSnail(moveTime));
    }

    // Update is called once per frame

    IEnumerator moveSnail(float moveTime)
    {

        for (float t = 0f; t < moveTime; t += Time.deltaTime)
        {
            float normalizedTime = t / moveTime;
            transform.position = Vector3.Lerp(startPos, endPos, normalizedTime);
            float dist = Vector3.Distance(transform.position, endPos);
            if (dist < 0.1f)
            {
                sounds[ranStair].Stop();
                transform.parent.GetComponent<BallGenerator>().destroyAtIndex(transform.GetSiblingIndex());
            }
            yield return null;
        }

    }

    int getRanNum()
    {
        return UnityEngine.Random.Range(0, 100);
    }
}