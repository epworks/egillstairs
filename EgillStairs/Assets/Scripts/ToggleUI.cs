﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleUI : MonoBehaviour
{
    private int uiIndex = 1;
    // Use this for initialization
    GameObject pMapPopup;
    GameObject stairPopup;
    GameObject statusPopup;
    GameObject ballsetPopup;

    Toggle ballsetToggle01;
    Toggle ballsetToggle02;
    Toggle ballsetToggle03;

    void Start()
    {
        pMapPopup = GameObject.Find("ProjMapPopup");
        stairPopup = GameObject.Find("StairGenPopup");
        statusPopup = GameObject.Find("StatusPopup");
        ballsetPopup = GameObject.Find("BallsetStatusPopup");
        pMapPopup.SetActive(false);
        stairPopup.SetActive(false);
        statusPopup.transform.position = new Vector3(-10000, statusPopup.transform.position.y, 0);

        initToggle();

        ballsetPopup.transform.position = new Vector3(-10000, statusPopup.transform.position.y, 0);

    }

    void initToggle()
    {
        // Debug.Log("name: " + GameObject.Find("Toggle_Set01").transform.name);
        ballsetToggle01 = GameObject.Find("Toggle_Set01").GetComponent<Toggle>();
        ballsetToggle02 = GameObject.Find("Toggle_Set02").GetComponent<Toggle>();
        ballsetToggle03 = GameObject.Find("Toggle_Set03").GetComponent<Toggle>();
        //Add listener for when the state of the Toggle changes, and output the state
        ballsetToggle01.onValueChanged.AddListener(delegate
        {
            ToggleValueChanged(ballsetToggle01);
        });

        //Add listener for when the state of the Toggle changes, and output the state
        ballsetToggle02.onValueChanged.AddListener(delegate
        {
            ToggleValueChanged(ballsetToggle02);
        });

        //Add listener for when the state of the Toggle changes, and output the state
        ballsetToggle03.onValueChanged.AddListener(delegate
        {
            ToggleValueChanged(ballsetToggle03);
        });
    }
    // Update is called once per frame

    void ToggleValueChanged(Toggle toggle)
    {
        BallGenerator ballGen = GameObject.Find("BallGenerator").GetComponent<BallGenerator>();
        //turn all off
        // ballsetToggle01.isOn = false;
        // ballsetToggle02.isOn = false;
        // ballsetToggle03.isOn = false;
        //set again
        // toggle.isOn = true;
        if (toggle.isOn)
        {
            if (toggle.name == "Toggle_Set01")
            {
                ballGen.ballSetIndex = 0;
                ballsetToggle02.isOn = false;
                ballsetToggle03.isOn = false;
            }
            else if (toggle.name == "Toggle_Set02")
            {
                ballGen.ballSetIndex = 1;
                ballsetToggle01.isOn = false;
                ballsetToggle03.isOn = false;
            }
            else if (toggle.name == "Toggle_Set03")
            {
                ballGen.ballSetIndex = 2;
                ballsetToggle01.isOn = false;
                ballsetToggle02.isOn = false;
            }
        }
    }

    void Update()
    {

        if (Input.GetKey("escape"))
        {
            Application.Quit();
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            UIVisibility();
            uiIndex = (uiIndex - 1) % 5;

        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            UIVisibility();
            uiIndex = (uiIndex + 1) % 5;

        }
    }
    void UIVisibility()
    {
        if (uiIndex == 0)
        {
            pMapPopup.SetActive(false);
            stairPopup.SetActive(false);
            ballsetPopup.transform.position = new Vector3(-10000, statusPopup.transform.position.y, 0);
            statusPopup.transform.position = new Vector3(-10000, statusPopup.transform.position.y, 0);
        }
        else if (uiIndex == 1)
        {
            pMapPopup.SetActive(true);
            stairPopup.SetActive(false);
            ballsetPopup.transform.position = new Vector3(-10000, statusPopup.transform.position.y, 0);
            statusPopup.transform.position = new Vector3(-10000, statusPopup.transform.position.y, 0);
        }
        else if (uiIndex == 2)
        {
            pMapPopup.SetActive(false);
            stairPopup.SetActive(false);
            ballsetPopup.transform.position = new Vector3(-10000, statusPopup.transform.position.y, 0);
            statusPopup.transform.position = new Vector3(300, statusPopup.transform.position.y, 0);
        }
        else if (uiIndex == 3)
        {
            ballsetPopup.transform.position = new Vector3(-10000, statusPopup.transform.position.y, 0);
            pMapPopup.SetActive(false);
            stairPopup.SetActive(true);
            statusPopup.transform.position = new Vector3(-10000, statusPopup.transform.position.y, 0);
        }
        else if (uiIndex == 4)
        {
            pMapPopup.SetActive(false);
            stairPopup.SetActive(false);
            ballsetPopup.transform.position = new Vector3(300, statusPopup.transform.position.y, 0);
            statusPopup.transform.position = new Vector3(-10000, statusPopup.transform.position.y, 0);
        }
    }
}
