﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class StairGenerator : MonoBehaviour
{

    [SerializeField]
    private GameObject cube;

    [SerializeField]
    public int numStairs = 16;

    [SerializeField]
    public float stairWidth = 2;
    [SerializeField]
    public float stairHeight = 0.15f;
    [SerializeField]
    public float stairDepth = 0.15f;
    public Vector3 absoluteCenter;
    List<GameObject> stairs = new List<GameObject>();
    // Use this for initialization
    void Start()
    {
        createStairs();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetStairDimensions()
    {
        foreach (GameObject stair in stairs)
        {
            Destroy(stair);
        }
        stairs.Clear();
        GameObject settings = GameObject.Find("StairGenPopup");
        stairWidth = float.Parse(settings.transform.Find("Width").transform.GetComponent<InputField>().text);
        stairDepth = float.Parse(settings.transform.Find("Depth").transform.GetComponent<InputField>().text);
        stairHeight = float.Parse(settings.transform.Find("Height").transform.GetComponent<InputField>().text);
        createStairs();
    }

    void createStairs()
    {
        Transform stairsContainer = transform.Find("Stairs");
        for (int i = 0, j=0; j < numStairs; i+=2, j++)
        {
            float stairPosY = stairHeight * j;
            float stairPosZ = stairDepth * j;
            float stairLipPosY = (stairPosY + stairHeight/2) - 0.0175f;
            float stairLipDepth = (stairDepth*2)+0.03f;
            float stairLipHeight = 0.035f;

            stairs.Add(Instantiate(cube, transform.position, Quaternion.identity, stairsContainer));
            stairs.Add(Instantiate(cube, transform.position, Quaternion.identity, stairsContainer));
            stairs[i].transform.name = "-".ToString();
            stairs[i + 1].transform.name = j.ToString();
            Vector3 position = new Vector3(transform.position.x, transform.position.y, transform.position.z);
            stairs[i].transform.localScale = new Vector3(stairWidth, stairHeight, stairDepth * 2); //make stair depth double so ball doesnt fall through cracks
            stairs[i].transform.localPosition = new Vector3(stairs[i].transform.localPosition.x, stairPosY, stairPosZ + stairDepth);
            stairs[i + 1].transform.localScale = new Vector3(stairWidth, stairLipHeight, stairLipDepth); //make stair depth double so ball doesnt fall through cracks
            stairs[i + 1].transform.localPosition = new Vector3(stairs[i+1].transform.localPosition.x, stairLipPosY, stairPosZ + stairDepth);
            //manually create top floor lip
            // if(i==numStairs-1){
            // stairs.Add(Instantiate(cube, transform.position, Quaternion.identity, stairsContainer));
            // stairs[i + 2].transform.localScale = new Vector3(stairWidth, stairLipHeight, stairLipDepth); //make stair depth double so ball doesnt fall through cracks
            // stairs[i + 2].transform.localPosition = new Vector3(stairs[i+2].transform.localPosition.x, stairLipPosY+stairHeight, stairPosZ + stairDepth+stairDepth);

            // }
        }
        //set center marker
        GameObject.Find("CenterMarker").transform.position = new Vector3(0, stairHeight * ((float)numStairs / 8f), stairDepth * (float)numStairs / 2);
        absoluteCenter = new Vector3(0, stairHeight * ((float)numStairs / 2f), stairDepth * (float)numStairs / 2);

    }
}

